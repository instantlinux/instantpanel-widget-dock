import * as libwnck_window_listener from 'libwnck-window-listener';
import * as fs from 'fs';

let wins: Array<XWindow>;

interface XWindow {
    xid: number;
    title: string;
    image: string;
    domelement?: HTMLElement;
    active: boolean;
};

let docklist: HTMLElement;

export default function createWidget(widgetfolder: string, isvertical: boolean) {
    // Main widget container
    let dockcontainer = document.createElement("div");
    dockcontainer.style.position  = "absolute";
    dockcontainer.style.top       = "0";
    dockcontainer.style.left      = "0";
    dockcontainer.style.right     = "0";
    dockcontainer.style.bottom    = "0";

    docklist = document.createElement("ul");
    docklist.style.position  = "absolute";
    docklist.style.top       = "50%";
    docklist.style.left      = "0";
    docklist.style.right     = "0";
    docklist.style.listStyle = "none";
    docklist.style.height    = "55px";
    docklist.style.maxHeight = "100%";
    docklist.style.transform = "translate(0, -50%)";
    docklist.className       = "windowlist";

    wins = [];

    let styles = document.createElement("style");
    styles.innerHTML = `.windowlist li {
        display: inline-block;
        margin: 0 3px;
        position: relative;
        width: 50px;
        height: 100%;
        text-align: center;
        transition: 0.1s;
    }
    .windowlist li:hover {
        transform: scale(1.125);
        cursor: pointer;
    }
    .windowlist li img {
        width: 40px;
        margin: 5px 0;
        transition: 0.1s;
    }
    .windowlist li:hover img {
        filter: brightness(80%);
    }
    .windowlist li.activeWindow::after {
        content: ' ';
        position: absolute;
        top: 47px;
        left: 50%;
        height: 4px;
        width: 4px;
        transform: translate(-50%, 0);
        border-radius: 4px;
        background: #3498db;
        box-shadow: 0 0 2px rgba(22, 160, 133, 0.7);
    }`;

    wins = [];

    libwnck_window_listener.SetCallbacks(function(name: string, icon: string, xid: string) {
        wins.push({
            xid: parseInt(xid),
            title: name,
            image: icon,
            active: false
        });
        updateWindows();
    }, function(name: string, icon: string, xid: string) {
        wins.forEach(element => {
            if (element.xid == parseInt(xid)) {
                element.active = true;
            } else {
                element.active = false;
            }
        });
        updateWindows();
    }, function(xid: string) {
        wins.forEach(element => {
            if (element.xid == parseInt(xid)) {
                wins.splice(wins.indexOf(element), 1);
            }
        });
        updateWindows();
    });

    dockcontainer.appendChild(styles);
    dockcontainer.appendChild(docklist);

    return dockcontainer;
}

function updateWindows() {
    docklist.innerHTML = "";
    wins.forEach(window => {
        let windowentry = document.createElement("li");
        if (window.active) {
            windowentry.className = "activeWindow";
        }
        let windowicon = document.createElement("img");
        windowicon.src = "data:image/png;base64," + window.image;
        windowentry.appendChild(windowicon);
        windowentry.title = window.title;
        windowentry.onclick = function() {
            if (window.active) {
                libwnck_window_listener.MinimizeWindow(window.xid.toString());
                window.active = false;
                updateWindows();
            } else {
                libwnck_window_listener.FocusWindow(window.xid.toString());
            }
        };
        window.domelement = windowentry;
        docklist.appendChild(windowentry);
    });
}