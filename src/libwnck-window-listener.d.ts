declare module "libwnck-window-listener" {
    export function SetCallbacks(cbOpened: Function, cbActive: Function, cbClosed: Function): any;
    export function FocusWindow(winid: string): any;
    export function MinimizeWindow(winid: string): any;
}